// import './App.css';
import FooterPlayer from './Components/Footer0';
import Streamio from './Components/Page1';
import Signin from "./Components/Signin";
import HomeScreen from './Home';
function App() {
  return (
    <>

      {/* <Signin /> */}

      <HomeScreen />
      <FooterPlayer />
      <Streamio />

    </>
  );
}

export default App;

// Streamio.js (a React component)

import React from 'react';

function Streamio() {
  return (
    <div className="streamio-container">
      {/* Navigation Bar */}
      <nav>
        <ul>
          <li>Home</li>
          <li>Explore</li>
          <li>Albums</li>
          <li>Artists</li>
          <li>Podcasts</li>
        </ul>
      </nav>

      {/* Main Content */}
      <div className="main-content">
        <img src="path/to/headphones-image.jpg" alt="Person with headphones" />
        <h1>Streamio</h1>
        <p>Where Music Comes to Life</p>
        <button>Buy Now</button>
        <button>Explore</button>
      </div>
    </div>
  );
}

export default Streamio;

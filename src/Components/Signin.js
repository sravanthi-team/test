import React from 'react'

export default function Signin() {
    const container = document.getElementById("container");
    const registerBtn = document.getElementById("register");
    const loginBtn = document.getElementById("login");

    registerBtn.addEventListener("click", () => {
        container.classList.add("active");
    });

    loginBtn.addEventListener("click", () => {
        container.classList.remove("active");
    });
    return (
        <>
            <div>
                <div class="container" id="container">
                    <div class="form-container sign-up">
                        <form>
                            <h1>Create Account</h1>
                            <div class="social-icons">
                                <a href="#" class="icon">
                                    <i class="fa-brands fa-google-plus-g"></i>
                                </a>
                                <a href="#" class="icon">
                                    <i class="fa-brands fa-facebook-f"></i>
                                </a>
                                <a href="#" class="icon">
                                    <i class="fa-brands fa-github"></i>
                                </a>
                                <a href="#" class="icon">
                                    <i class="fa-brands fa-linkedin-in"></i>
                                </a>
                            </div>
                            <span>or use your email for registration</span>
                            <input type="text" placeholder="name" />
                            <input type="email" placeholder="Email" />
                            <input type="password" placeholder="password" />
                            <button>Sign Up</button>
                        </form>
                    </div>
                    <div class="form-container sign-in">
                        <form>
                            <h1>Sign in</h1>
                            <div class="social-icons">
                                <a href="#" class="icon">
                                    <i class="fa-brands fa-google-plus-g"></i>
                                </a>
                                <a href="#" class="icon">
                                    <i class="fa-brands fa-facebook-f"></i>
                                </a>
                                <a href="#" class="icon">
                                    <i class="fa-brands fa-github"></i>
                                </a>
                                <a href="#" class="icon">
                                    <i class="fa-brands fa-linkedin-in"></i>
                                </a>
                            </div>
                            <span>or use your Email password</span>
                            <input type="email" placeholder="Email" />
                            <input type="password" placeholder="password" />
                            <a href="#">Forgot Your Password</a>
                            <button>Log in</button>
                        </form>
                    </div>
                    <div class="toggle-container">
                        <div class="toggle">
                            <div class="toggle-panel toggle-left">
                                <h1>Welcome back</h1>
                                <p>Enter Your personel Details to use all of site Features</p>
                                <button class="hidden" id="login">log in</button>
                            </div>
                            <div class="toggle-panel toggle-right">
                                <h1>Hello Dear Customer</h1>
                                <p>Register with your personal details to use all of site features</p>
                                <button class="hidden" id="register">Sign Up</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
